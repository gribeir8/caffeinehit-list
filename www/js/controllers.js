var app = angular.module('caffeinehit.controllers', []);

app.controller("YelpController", function ($scope, YelpService) {
	$scope.yelp = YelpService;

	$scope.doRefresh = function(){
		if(!$scope.yelp.isLoading){
			var promisse = $scope.yelp.refresh();

			promisse.then( function(){

				$scope.$broadcast('scroll.refreshComplete');
			}, function(){

				console.log('Error during get request.');
			});
		}
	};

	$scope.loadMore = function(){

		console.log('load more');
		if(!$scope.yelp.isLoading && $scope.yelp.hasMore){
			var promisse = $scope.yelp.next();

			promisse.then( function(){

				console.log('hasMore: ' + $scope.yelp.hasMore)
				$scope.$broadcast('scroll.infiniteScrollComplete');
			}, function(){

				console.log('Error during get request.');
			});
		}
	};

	$scope.getDirections = function(cafe){
		console.log('getting directions');

		var destination = [
			cafe.location.coordinate.latitude
			,cafe.location.coordinate.longitude
		];

		var origin = [
			$scope.yelp.lat
			,$scope.yelp.lon
		];

		launchnavigator.navigate(destination, origin);
	};

	$scope.openMap = function(cafe){
		console.log('openning Map');
	};

});
